use std::thread;
use std::sync::{Arc, RwLock};
use std::sync::mpsc::Receiver;
use std::collections::HashMap;

use ws;
use serde_json;
use uuid::Uuid;

#[derive(Serialize)]
struct Payload(Vec<f32>);

struct Handler {
    conns: Arc<RwLock<HashMap<String, ws::Sender>>>,
    uuid: String,
}

impl ws::Handler for Handler {
    fn on_close(&mut self, _code: ws::CloseCode, _reason: &str) {
        let mut conns = self.conns.write().unwrap();
        (*conns).remove(&self.uuid);
    }
}

pub fn websocket(payload_rx: Receiver<Vec<f32>>) {
    let connections = Arc::new(RwLock::new(HashMap::new()));
    let conns = connections.clone();

    let _ws_sender = thread::spawn(move || {
        for payload in payload_rx {
            // println!("got payload {:?}", payload);
            let payload = Payload(payload);
            let payload = serde_json::to_string(&payload).unwrap();
            let conns = conns.read().unwrap();

            (*conns).values().for_each(|sender: &ws::Sender| {
                let _ = sender.send(payload.clone());
            });
        }
    });

    let conns = connections.clone();
    let _ = ws::listen("127.0.0.1:3012", |out| {
        let uuid = Uuid::new_v4().to_string();
        let mut conns = conns.write().unwrap();
        (*conns).insert(uuid.clone(), out);
        Handler { conns: connections.clone(), uuid }
    });
}
