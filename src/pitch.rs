use ::frequency::Frequency;

pub struct Pitch(pub usize);

impl From<Frequency> for Pitch {
    fn from(Frequency(f): Frequency) -> Pitch {
        let pitch = (12.0 * (f/440.0).log2() + 69.0).max(0.0).min(127.0).round();
        Pitch(pitch as usize)
    }
}
