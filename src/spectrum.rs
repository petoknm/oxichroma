use ::pitch::Pitch;
use ::bin::Bin;
use ::frequency::Frequency;
use rustfft::num_complex::Complex;

#[derive(Debug)]
pub struct Spectrum {
    sample_rate: f32,
    buffer_size: usize,
    data: Vec<f32>,
}

impl Spectrum {
    pub fn from_complex(sample_rate: f32, values: &[Complex<f32>]) -> Spectrum {
        Spectrum {
            sample_rate,
            buffer_size: values.len(),
            data: values.iter().take(values.len() / 2).map(|c| c.norm_sqr()).collect()
        }
    }

    fn pitches(&self) -> Vec<f32> {
        let mut pitches = vec![0.0; 128];
        for (i, v) in self.data.iter().enumerate() {
            let f: Frequency = Bin{ index: i, sample_rate: self.sample_rate, buffer_size: self.buffer_size }.into();
            let Pitch(index) = f.into();
            pitches[index] += v;
        }
        pitches
    }

    pub fn chromas(&self) -> Vec<f32> {
        let pitches = self.pitches();
        let mut chromas = vec![0.0; 12];
        for (i, v) in pitches.iter().enumerate() {
            chromas[i % 12] += v;
        }
        // println!("{:0.1?}", chromas);
        chromas
    }
}
