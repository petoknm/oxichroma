use std::ops::{Add, Sub, Div};

pub struct RcFilter<T: Add<T, Output=T> + Sub<T, Output=T> + Div<f32, Output=T> + Clone> {
    value: T,
    time_constant: f32,
}

impl<T: Add<T, Output=T> + Sub<T, Output=T> + Div<f32, Output=T> + Clone> RcFilter<T> {
    pub fn new(value: T, time_constant: f32) -> RcFilter<T> {
        RcFilter{ value, time_constant }
    }

    pub fn push(&mut self, value: T) -> T {
        let diff = value - self.value.clone();
        self.value = self.value.clone() + diff / self.time_constant;
        self.value.clone()
    }
}
