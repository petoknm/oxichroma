use ::bin::Bin;

pub struct Frequency(pub f32);

impl From<Bin> for Frequency {
    fn from(Bin{index, sample_rate, buffer_size}: Bin) -> Frequency {
        Frequency((index as f32) * sample_rate / buffer_size as f32)
    }
}
