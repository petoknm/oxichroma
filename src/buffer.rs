pub struct Buffer {
    window_length: usize,
    hop_size: usize,
    samples: Vec<f32>,
}

impl Buffer {
    pub fn new(window_length: usize, hop_size: usize) -> Buffer {
        Buffer{
            window_length, hop_size,
            samples: vec![]
        }
    }

    pub fn push(&mut self, mut samples: Vec<f32>) {
        self.samples.append(&mut samples);
    }

    pub fn next_window(&mut self) -> Option<Vec<f32>> {
        if self.samples.len() >= self.window_length {
            let window = (&self.samples[..self.window_length]).to_vec();
            self.samples = (&self.samples[self.hop_size..]).to_vec();
            Some(window)
        } else {
            None
        }
    }
}
