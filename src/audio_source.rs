use std::{thread, time};
use std::sync::mpsc::Sender;

use jack::{Client, ClosureProcessHandler, ProcessScope, Control, AudioIn};

pub fn audio_source(client: Client, samples_tx: Sender<Vec<f32>>) {
    let port = client.register_port("input", AudioIn).unwrap();

    let process_handler = move |_client: &Client, scope: &ProcessScope| {
        let _ = samples_tx.send(port.as_slice(scope).to_owned());
        Control::Continue
    };

    let _active_client = client.activate_async((), ClosureProcessHandler::new(process_handler)).unwrap();
    thread::sleep(time::Duration::from_millis(1000*60*60*24));
}
