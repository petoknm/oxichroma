use std::ops::Div;
use num::traits::Num;

pub fn average<T: Num + Div<f32, Output=T> + Clone>(values: &[T]) -> T {
    let sum = values.iter()
        .fold(T::zero(), |a, c| a + c.clone());

    sum / values.len() as f32
}
