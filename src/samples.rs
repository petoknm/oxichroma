use rustfft::num_complex::Complex;

pub struct Samples(pub Vec<f32>);

impl Samples {
    pub fn apply_window(&mut self, window: &[f32]) {
        for i in 0..self.0.len() {
            self.0[i] *= window[i];
        }
    }

    pub fn to_complex(&self) -> Vec<Complex<f32>> {
        self.0.iter().map(|&f| Complex::new(f, 0.0)).collect()
    }
}
