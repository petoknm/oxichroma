#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;

extern crate num;
extern crate rustfft;
extern crate jack;
extern crate ws;
extern crate palette;
extern crate pid_control;
extern crate uuid;
extern crate ndarray;

use std::thread;
use std::sync::mpsc::channel;

use jack::{Client, ClientOptions};

mod analysis;
mod audio_source;
mod buffer;
mod rc_filter;
mod samples;
mod bin;
mod frequency;
mod pitch;
mod spectrum;
mod chromagram;
mod output;
mod window;

pub use analysis::*;
pub use audio_source::*;
pub use buffer::*;
pub use rc_filter::*;
pub use samples::*;
pub use bin::*;
pub use frequency::*;
pub use pitch::*;
pub use spectrum::*;
pub use chromagram::*;
// pub use output::*;
pub use window::*;

fn main() {
    let (samples_tx, samples_rx) = channel();
    let (chromas_tx, chromas_rx) = channel();

    let (client, _status) = Client::new("oxichroma", ClientOptions::NO_START_SERVER).unwrap();
    let sample_rate = client.sample_rate() as f32;
    let buffer_size = client.buffer_size() as usize;

    println!("Initializing with sample rate {} and buffer size {}", sample_rate, buffer_size);

    let _jack = thread::spawn(move || audio_source(client, samples_tx));
    let _fft = thread::spawn(move || analysis(sample_rate, buffer_size, samples_rx, chromas_tx));

    output::websocket(chromas_rx);
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn bin2freq() {
        let Frequency(f) = Bin{ index: 10, sample_rate: 44100.0, buffer_size: 1024 }.into();
        assert!(f < 450.0 && f > 420.0);
    }
}
