use palette::{Srgb, Hsv, IntoColor, LinSrgb};

trait Sigmoid {
    fn sigmoid(self) -> Self;
}

impl Sigmoid for f32 {
    fn sigmoid(self) -> f32 {
        1.0 / (1.0 + 10.0f32.powf(-self))
    }
}

#[derive(Debug)]
pub struct Chromagram(pub Vec<f32>);

impl Chromagram {
    pub fn color(&self, average_power: f32) -> Srgb {
        // println!("average_power {:.3}", average_power);
        let lin_srgb = self.0.iter().enumerate()
            .map(|(i, &v)| Hsv::new(360.0 * (i as f32) / 12.0, 1.0, ((v / average_power).log2() - 2.0).sigmoid()))
            .map(|hsv| hsv.into_rgb())
            .fold(LinSrgb::new(0.0, 0.0, 0.0), |a, c| a + c);
        // println!("{:?}", lin_srgb);
        Srgb::from_linear(lin_srgb)
    }

    pub fn average_power(&self) -> f32 {
        self.0.iter().sum::<f32>() / 12.0
    }
}
