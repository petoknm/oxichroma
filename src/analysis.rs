use {Buffer, Samples, RcFilter, Spectrum, Chromagram, hann_window};

use std::sync::mpsc::{Receiver, Sender};

use rustfft::FFTplanner;
use rustfft::num_complex::Complex;
use rustfft::num_traits::{Zero};

use ndarray::Array1;

pub fn analysis(sample_rate: f32, buffer_size: usize, samples_rx: Receiver<Vec<f32>>, chromas_tx: Sender<Vec<f32>>) {

    let analysis_buffer_size = buffer_size * 4;

    let mut planner = FFTplanner::new(false);
    let fft = planner.plan_fft(analysis_buffer_size);
    let window = hann_window(analysis_buffer_size);

    let mut buffer = Buffer::new(analysis_buffer_size, analysis_buffer_size / 4);
    let mut smoothing_filter = RcFilter::new(vec![0.0; 12].into(), 15.0);
    let mut power_filter = RcFilter::new(0.0, 100.0);

    for samples in samples_rx {
        // println!("got samples");

        buffer.push(samples);
        while let Some(samples) = buffer.next_window() {
            // println!("got window");

            let mut samples = Samples(samples);
            samples.apply_window(&window);

            let mut input = samples.to_complex();
            let mut output = vec![Complex::<f32>::zero(); analysis_buffer_size];
            fft.process(&mut input, &mut output);

            let spectrum = Spectrum::from_complex(sample_rate, &output);
            // println!("{:.0?}", spectrum);
            let chromas: Array1<_> = spectrum.chromas().into();

            let average_power = {
                let chromagram = Chromagram(chromas.clone().into_raw_vec());
                let power = chromagram.average_power();
                power_filter.push(power)
            };

            let srgb = {
                let smoothed_chromas = smoothing_filter.push(chromas);
                let chromagram = Chromagram(smoothed_chromas.into_raw_vec());
                // println!("{:.3?}", chromagram);
                chromagram.color(average_power)
            };

            let (mut r, mut g, mut b) = srgb.into_components();

            r = r.min(1.0);
            g = g.min(1.0);
            b = b.min(1.0);

            let _ = chromas_tx.send(vec![r, g, b]);
        }
    }
}
