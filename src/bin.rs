pub struct Bin {
    pub index: usize,
    pub sample_rate: f32,
    pub buffer_size: usize,
}
