use std::f32::consts::PI;

use rustfft::num_traits::Pow;

pub fn hann_window(len: usize) -> Vec<f32> {
    (0..len)
        .map(|i| {
            let mut i = i as f32;
            i *= PI / (len - 1) as f32;
            i.sin().pow(2)
        })
        .collect()
}
