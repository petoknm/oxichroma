import React, { Component } from 'react';
import './App.css';

class App extends Component {

  constructor(){
    super();
    this.state = {
      color: [0,0,0],
      ws: null,
    };
  }

  componentDidMount() {
    const ws = new WebSocket('ws://127.0.0.1:3012/');
    ws.addEventListener('message', event => {
      const { data } = event;
      const color = JSON.parse(data);
      this.setState({color});
    });
    this.setState({ws});
  }

  componentWillUnmount() {
    const { ws } = this.state;
    ws.close();
  }

  render() {
    const { color } = this.state;
    const [r, g, b] = color;

    return (
      <div>
      <p style={{backgroundColor: `rgb(${256*r}, ${256*g}, ${256*b})`, height: '500px'}}>
        Hello
      </p>
      { r }
      <br/>
      { g }
      <br/>
      { b }
      </div>
    );
  }
}

export default App;
